<table>
<thead>
<tr>
<td><b>module</b></td>
<td><b>what it does</b></td>
</tr>
</thead>
<tr>
<td>budget.py</td>
<td>definitions for FinancialOperation and Budget</td>
</tr>
<tr>
<td>charts.py</td>
<td>all of the chart functionality</td>
</tr>
<tr>
<td>cli.py</td>
<td>implements handlers for the CLI lbud client</td>
</tr>
<tr>
<td>constants.py</td>
<td>constants used by the package</td>
</tr>
<tr>
<td>display.py</td>
<td>classes that help display information in key-value kind of way</td>
</tr>
<tr>
<td>helpers.py</td>
<td>various helper functions that operate on primitives</td>
</tr>
<tr>
<td>main.py</td>
<td>the source of lbud executable</td>
</tr>
<tr>
<td>money_provider.py</td>
<td>module provides all the needed classes and functions for creation and handling of money</td>
</tr>
<tr>
<td>stats.py</td>
<td>statistics that are calculated from the budget instance and a list of operations</td>
</tr>
</table>
