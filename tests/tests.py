from datetime import date
from decimal import Decimal
from typing import List

import pytest
from freezegun import freeze_time

from lazy_budget.budget import Budget, FinancialOperation
from lazy_budget.money_provider import Currency, Money, parse_money
from lazy_budget.stats import BasicBudgetStats


@pytest.mark.parametrize(
    "stats,frozen_date",
    (
        (
            BasicBudgetStats(
                today=date(2021, 3, 10),
                total_days=10,
                days_left=9,
                available_per_day=Money("150", Currency.PLN),
                can_spend_today=Money("140", Currency.PLN),
                avg_spent_per_day=Money("10", Currency.PLN),
                currently_keeping=Money("640", Currency.PLN),
                currency=Currency.PLN,
                spent_today=Money("10", Currency.PLN),
                total_available=Money("1990", Currency.PLN),
                total_spent=Money("10", Currency.PLN),
                maximum_spent=Money("10", Currency.PLN),
            ),
            "2021-03-10",
        ),
        (
            BasicBudgetStats(
                today=date(2021, 3, 11),
                total_days=10,
                days_left=8,
                available_per_day=Money("150", Currency.PLN),
                can_spend_today=Money("290", Currency.PLN),
                avg_spent_per_day=Money("5", Currency.PLN),
                currently_keeping=Money("790", Currency.PLN),
                currency=Currency.PLN,
                spent_today=Money("0", Currency.PLN),
                total_available=Money("1990", Currency.PLN),
                total_spent=Money("10", Currency.PLN),
                maximum_spent=Money("10", Currency.PLN),
            ),
            "2021-03-11",
        ),
        (
            BasicBudgetStats(
                today=date(2021, 3, 12),
                total_days=10,
                days_left=7,
                available_per_day=Money("150", Currency.PLN),
                can_spend_today=Money("440", Currency.PLN),
                avg_spent_per_day=Money("3.33", Currency.PLN),
                currently_keeping=Money("940", Currency.PLN),
                currency=Currency.PLN,
                spent_today=Money("0", Currency.PLN),
                total_available=Money("1990", Currency.PLN),
                total_spent=Money("10", Currency.PLN),
                maximum_spent=Money("10", Currency.PLN),
            ),
            "2021-03-12",
        ),
    ),
)
def test_integration(
    budget: Budget,
    financial_operations: List[FinancialOperation],
    stats: BasicBudgetStats,
    frozen_date: date,
):
    with freeze_time(frozen_date) as frozen_date_:
        budget.today = frozen_date_().date()
        stats_test = BasicBudgetStats.get_stats(budget, financial_operations)
        assert stats == stats_test


@pytest.mark.parametrize(
    "money_str,parse_result,default_currency",
    (
        ("-10", Money("-10", Currency.USD), Currency.USD),
        ("-10 PLN", Money("-10", Currency.PLN), Currency.USD),
        ("-10 PLN", Money("-10", Currency.PLN), Currency.USD),
        ("14.30 UAH", Money("14.30", Currency.UAH), Currency.USD),
        ("39.99", Money("39.99", Currency.PLN), Currency.PLN),
    ),
)
def test_parse_money(money_str, parse_result, default_currency):
    assert parse_money(money_str, default_currency) == parse_result


@pytest.mark.parametrize(
    "current_date, result",
    (
        (date(2021, 3, 10), Money("-10", Currency.PLN)),
        (date(2021, 3, 11), Money("0", Currency.PLN)),
        (date(2021, 3, 12), Money("0", Currency.PLN)),
        (date(2021, 3, 13), Money("-1000", Currency.PLN)),
        (date(2021, 3, 14), Money("0", Currency.PLN)),
        (date(2021, 3, 15), Money("0", Currency.PLN)),
        (date(2021, 3, 16), Money("-5", Currency.PLN)),
        (date(2021, 3, 17), Money("0", Currency.PLN)),
        (date(2021, 3, 18), Money("0", Currency.PLN)),
        (date(2021, 3, 19), Money("0", Currency.PLN)),
    ),
)
def test_get_spent_today(
    budget: Budget,
    financial_operations: List[FinancialOperation],
    current_date: date,
    result: Money,
):
    budget.today = current_date
    by_day = budget.get_spent_by_day(financial_operations)
    assert by_day[current_date] == result


def test_weird_can_spend_today_bug(
    weird_budget: Budget, weird_operations: List[FinancialOperation]
):
    stats_test = BasicBudgetStats.get_stats(weird_budget, weird_operations)
    assert stats_test.available_per_day.amount == Decimal("89.52")
    assert stats_test.can_spend_today.amount == Decimal("89.14")


@pytest.mark.parametrize(
    ("raw_description", "description", "category"),
    (
        ("123", "123", None),
        ("[abc]", "", "abc"),
        ("[piwo]  Żubr Eksport", "Żubr Eksport", "piwo"),
        ("[] \t no cat", "no cat", None),
    ),
)
def test_parse_description(raw_description: str, description: str, category: str):
    assert FinancialOperation.parse_description(raw_description) == (
        category,
        description,
    )
