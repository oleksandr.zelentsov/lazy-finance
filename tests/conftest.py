from datetime import date, datetime
from typing import List

import pytest

from lazy_budget.budget import Budget, FinancialOperation
from lazy_budget.money_provider import Currency, Money

PLN = Currency.PLN


@pytest.fixture
def financial_operations() -> List[FinancialOperation]:
    return [
        FinancialOperation(
            money_value=Money("-5", PLN),
            dttm=datetime(2021, 3, 10),
        ),
        FinancialOperation(
            money_value=Money("-5", PLN),
            dttm=datetime(2021, 3, 10),
        ),  # 10: -10
        FinancialOperation(
            money_value=Money("-5", PLN),
            dttm=datetime(2021, 3, 11),
        ),
        FinancialOperation(
            money_value=Money("5", PLN),
            dttm=datetime(2021, 3, 11),
        ),  # 11: 0
        # 12: 0
        FinancialOperation(
            money_value=Money("-1000", PLN),
            dttm=datetime(2021, 3, 13),
        ),  # 13: -1000
        # 14: 0
        # 15: 0
        FinancialOperation(
            money_value=Money("-5", PLN),
            dttm=datetime(2021, 3, 16),
        ),  # 16: -5
        # 17: 0
        # 18: 0
        # 19: 0
    ]


@pytest.fixture
def budget() -> Budget:
    return Budget(
        total=Money("2000", PLN),
        keep=Money("500", PLN),
        start=date(2021, 3, 10),
        end=date(2021, 3, 19),
        currency=PLN,
    )
    # per day: (2000 - 500) / 10 = 150


@pytest.fixture
def weird_budget() -> Budget:
    return Budget(
        total=Money("4564.78", PLN),
        keep=Money("1700", PLN),
        start=date(2021, 5, 10),
        end=date(2021, 6, 10),
        currency=PLN,
        today=date(2021, 5, 11),
    )


@pytest.fixture
def weird_operations() -> List[FinancialOperation]:
    # -38.90,food,2021-05-11 17:14:32.505861
    # -51,podstawka pod gitarę,2021-05-11 17:14:48.968925
    return [
        FinancialOperation(
            money_value=Money("-38.90", PLN),
            description="food",
            dttm=datetime.fromisoformat("2021-05-11 17:14:32.505861"),
        ),
        FinancialOperation(
            money_value=Money("-51", PLN),
            description="podstawka pod gitarę",
            dttm=datetime.fromisoformat("2021-05-11 17:14:48.968925"),
        ),
    ]
