from locale import getdefaultlocale

DAYS_IN_A_SECOND = 0.0000115741
BUDGET_FILE = "./budget.yml"
HISTORY_FILE = "./history.csv"
DEFAULT_LOCALE = getdefaultlocale()[0]
CONFIG_FILE_PATH = "~/.lbudrc.yml"
